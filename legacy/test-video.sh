#!/bin/bash

echo "Tarjeta Gráfica :"
lspci | grep VGA | colrm 1 4 ;
echo 

echo "Procesador :"
cat /proc/cpuinfo | egrep "model name|MHz" ;
echo 

echo "Pantalla :"
xdpyinfo | egrep "version:|dimensions|depth of" ;
echo 

echo "GLX Info :"
glxinfo | egrep -A2 "direct rendering|OpenGL vendor" ; uname -sr;
echo

echo "FPS :"
glxgears & sleep 30 ; killall glxgears
echo 
