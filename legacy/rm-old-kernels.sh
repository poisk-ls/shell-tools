#!/bin/bash

INSTALLED_KERNELS=$(dpkg -l linux-image-3\* | grep ^ii | awk '{ print $2 "_" $3 }' | sed -e 's/linux-image-//g')
INSTALLED_HEADERS=$(dpkg -l linux-headers-3\* | grep ^ii | awk '{ print $2 "_" $3 }' | sed -e 's/linux-headers-//g')
RUNNING_KERNEL_NAME=$(uname -a | awk '{print $3}')


for kernelpkg in $INSTALLED_KERNELS ; do
    if [ $RUNNING_KERNEL_NAME == ${kernelpkg%_*} ] ; then
        RUNNING_KERNEL_VERSION=${kernelpkg#*_}
    fi
done

echo "Version of current kernel package: $RUNNING_KERNEL_VERSION"


for kernelpkg in $INSTALLED_KERNELS ; do                                 
    if $(dpkg --compare-versions $RUNNING_KERNEL_VERSION gt ${kernelpkg#*_}) ; then
        echo "Running: apt-get purge linux-image-${kernelpkg#*_}"
        apt-get purge linux-image-${kernelpkg%_*}
    else
        echo "Not purging linux-image-${kernelpkg%_*}, this kernel is newer than or equal to the currently running one."
    fi
done

for kernelpkg in $INSTALLED_HEADERS ; do                                 
    if $(dpkg --compare-versions $RUNNING_KERNEL_VERSION gt ${kernelpkg#*_}) ; then
        echo "Running: apt-get purge linux-image-${kernelpkg#*_}"
        apt-get purge linux-headers-${kernelpkg%_*}
    else
        echo "Not purging linux-headers-${kernelpkg%_*}, these kernel headers are newer than or equal to the current one in use."
    fi
done
