#!/bin/bash

# -----------------------------------------------------------------------------
# Wordpress installer (wp-cli).
# -----------------------------------------------------------------------------
# Installing Wordpress with wp-cli.
# -----------------------------------------------------------------------------
#
# @autor        David Martín <hola@dmar.dev>
# @copyright    2021 - David Martín
# @version      1.0.0
# @package      shell-tools
# -----------------------------------------------------------------------------

ver='1.0.0'

# ---

title='\033[45m'
green='\033[0;32m'
greenbg='\033[42m'
red='\033[0;31m'
redbg='\033[41m'
nc='\033[0m'

# ---

# Config
url='';
title="";
locale="es_ES";
tz="Europe/Madrid";
date="j \d\e F \d\e Y";
time="H:i";
auto_updater="";
core_updater="";
cache="true";
disable_cron="false";
memory="";
max_memory="";
with_htaccess=true

dbname="";
dbuser="";
dbpass="";
dbhost="localhost";
dbprefix="wp_"

admin_user="";
admin_display_name="";
admin_password="";
admin_email="";

plugins=""
theme=""
default_theme="twentytwentytwo"

# ---

printf "${title}install-wp.sh${nc} ${ver} - Install Wordpress. \n"

# Script expects to find wp-cli at PATH, renamed to wp.
printf "Checking if wp-cli is available ..... ";
if ! [ -x "$(command -v wp)" ]; then
    printf "${redbg}Error${nc}: Cant find wp-cli. \n"
    exit 1
else
    printf "${green} OK!! ${nc} \n \n"
fi

# ---

wp core download --locale=$locale --skip-content
wp config create --dbname=$dbname --dbuser=$dbuser --dbpass=$dbpass --dbhost=$dbhost --dbprefix=$dbprefix --locale=$locale --extra-php <<PHP
// ----------------------------------------------------------------------------
// User settings
// ----------------------------------------------------------------------------
define('WP_HOME', '$url');
define('WP_SITEURL', '$url');
// ---
define('WP_DEBUG', false);
define('WP_DEBUG_DISPLAY', false);
define('WP_DEBUG_LOG', true);
// ----
define('AUTOMATIC_UPDATER_DISABLED', $auto_updater);
define('WP_AUTO_UPDATE_CORE', $core_updater);
// ---
define('WP_CACHE', $cache);
// ---
define('DISABLE_WP_CRON', $disable_cron);
// ---
define('WP_MEMORY_LIMIT', '$memory');
define('WP_MAX_MEMORY_LIMIT', '$max_memory');
// ----------------------------------------------------------------------------
PHP

wp db create
wp core install --url=$url  --title="$title" --admin_user=$admin_user --admin_password=$admin_password --admin_email=$admin_email --skip-email

# Add plugins if filled.
if ! [ -z "$plugins" ]; then
    wp plugin install $plugins
    wp plugin activate $plugins
fi

# Add theme if filled.
if ! [ -z "$theme" ]; then
    wp theme install $theme
else
    wp theme install $default_theme
fi

# Config TZ and date/time formats.
wp option update timezone_string $tz
wp option update date_format $date
wp option update time_format $time

# Add .htaccess
if [[ "$with_htaccess" = true ]]; then
    touch .htaccess
    cat << EOF >> .htaccess
<IfModule mod_rewrite.c>
    RewriteEngine On
    RewriteCond %{HTTPS} !=on
    RewriteRule ^ https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]
</IfModule>
<Files xmlrpc.php>
    Order Deny,Allow
    Deny from all
</Files>
<Files wp-config.php>
    Order Deny,Allow
    Deny from all
</Files>
EOF
fi

exit 0
